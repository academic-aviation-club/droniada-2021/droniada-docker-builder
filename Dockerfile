FROM fedora:33

RUN dnf install --refresh -y \
    git \
    which \
    findutils

RUN dnf install --refresh -y \
    python3 \
    python3-pip \
    black \
    pylint \
    virtualenv \
    poetry

RUN dnf install --refresh -y \
    blender \
    python-flask

RUN dnf install --refresh -y \
    xorg-x11-server-Xvfb

RUN dnf install --refresh -y \
    gcc

RUN dnf install --refresh -y \
    python-devel
